# Hacking wifi with WPA2

A short presentation of how to hack into a WPA2 secured WiFi network using basic tools found in Kali Linux.

The initial document was created as an assignment project for TTZC0600 Cybersecurity -course (in Finnish) on 12.11.2017. This is a translated and somewhat shortened version.

## Basics

The objective of this assignment was to break into a WPA2 secured WiFi network, find out devices connected to it and attempt to load a file into one of the devices.

The two first objectives were completed.

Actual breaking into a device was not done, but some ways of how this could be accomplished were explored

TLDR: Breaking into a WPA2 secured WiFi is easy and does not require a significant amount of hacking skill nor knowhow, practically all of the steps can and were found online. Restricting allowed devices with MAC address is also ineffective way as it is possible and is fairly easy to spoof (fake) a MAC address.

Up until around mid October of 2017, WPA2 was considered a secure way to safeguard a WiFi network, around that time someone managed to break the encryption and extract a plaintext version of the WPA2 encrypted password by sniffing out packages sent to and from WiFi broadcast device.

This project will simply show, how easy it actually is to sniff out this information.

---

## Objective

The primary objective was to simply manage to get a password for WiFi network. There was a plethora of instructions found for this particular part.

The second part of the project was to sniff around inside the network and find out other devices connected to the same network. The tools found in Kali Linux by default, were more than capable to achieve this.

The third part was explored but not attempted as it could've resulted in infection of several devices. Ways to break into a device and plant something on it were explored theoretically.

---

## Equipment

This assignment does not really require any special devices, the only exception was a WiFi adapter capable of sending package-data required by some of the used software.

A MacBoook Pro 15 was used as an attacker with a connected Asus USB-N10 Nano WiFi adapter.

Samsung Galaxy S6 Edge was used as the Wifi access point.

MacBook Pro 13 was used at the victim machine.

---

## Software

Aircrack-ng -software suite, nmap as well as zenmap are all found from Kali Linux 2 version as a default.

### Ifconfig & ip link

These tools allow configuring of network adapters in Linux OS. In this case these tools were used to reconfigure the network adapters of the attacking machine as well as during MAC spoofing.

### Aircrack-ng software suite

Aircrack-ng software suite contains multiple tools to look for vulnerabilities of a WiFi network.

#### Airmon-ng

Is used for "listening" aka "monitoring" for WiFi signals. It shows active network adapters.

Use in this project:

    airmon-ng
    airmon-ng start|stop wlan0

Where

    start|stop  = starts | stops monitoring
    wlan0       = id of the network adapter

#### Airodump-ng

Listens and hijacks data-packets of a defined connection. Can also hijack location information. If a directory is provided can save hijacked information for later use. Data collected by Airodump-ng can for example be viewed with WireShark.

Use in this project:

    airodump-ng wlan0mon
    airodump-ng -c 6 --bssid E8:50:8B:59:77:8F -w /root/Desktop wlan0mon

Where

    -c 6                      = Defines which channel to listen to
    --bssid E8:50:8B:59:77:8F = defines the MAC address of a device to listen
    -w /root/Desktop          = defines directory where to save found data
    wlan0mon                  = the id of the listening network adapter

#### Aireplay-ng

Aireplay-ng is the attacking part of the aircrack-ng suite. It is capable of breaking the connection between the access point and a connected device (deauthenticate). The main purpose of this tool is to create traffic that can then be listened to and thus to collect data which can be later used to break password encryption.

Use in this project:

    aireplay-ng -0 2 -a E8:50:8B:59:77:8F -c AC:BC:32:A3:83:A9 wlan0mon

Where

    -0 2 = määrittää millainen hyökkäys ja kuinka monta kertaa se yritetään ajaa (deautentikointi x 2)
    -a E8:50:8B:59:77:8F = tukiaseman MAC-osoite
    -c AC:BC:32:A3:83:A9 = uhrikoneen MAC-osoite
    wlan0mon = valitun monitorointilaitteen tunniste

#### Aircrack-ng

### Machchanger

---

## Things noticed

---

## So how to protect your Wifi?

---

## Sources